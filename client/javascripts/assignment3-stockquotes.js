/*globals app, io*/
(function () {
    "use strict";

    window.app = {

        ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php",
        socket: io("http://server7.tezzt.nl:1333"),

        loadedDataEvent: null,
        stockdata: null,
        quoteContainer: null,

        settings: {
            refreshDelay: 3000,
            dataSource: ""
        },

        dataSource: function () {
            var parameter, parameterValue;
            parameter = window.location.href.split("?")[1];
            if (parameter) {
                parameterValue = parameter.split("=")[1];
                app.settings.dataSource = parameterValue;
            }
        },

        loadDataSource: function () {
            switch (app.settings.dataSource) {
            case "ajax":
                return app.getAjaxData();
            case "websocket":
                return app.getWebsocketData();
            case "generatedData":
                return app.randomData();
            default:
                return app.getAjaxData();
            }
        },

        XMLHttpRequest: function (method, url) {
            var request;
            request = new XMLHttpRequest();
            if (request.withCredentials !== undefined) {
                request.open(method, url, true);
            } else {
                request = null;
            }
            return request;
        },

        getWebsocketData: function () {
            app.socket.on('stockquotes', function (data) {
                app.stockdata = data.query.results.row;
                app.quoteContainer.dispatchEvent(app.loadedDataEvent);
            });
        },

        getAjaxData: function () {
            var request;
            request = app.XMLHttpRequest('GET', app.ajaxUrl);
            request.addEventListener("load", app.responseHandler);
            setTimeout(app.loadDataSource, app.settings.refreshDelay);
            request.send();
        },

        responseHandler: function (e) {
            var responseText = e.target.responseText;
            app.stockdata = JSON.parse(responseText).query.results.row;
            app.quoteContainer.dispatchEvent(app.loadedDataEvent);
        },

        randomData: function () {
            var data = [],
                arrayLength = 25,
                minDate = new Date(2014, 0, 1),
                maxDate = new Date(),
                i;

            for (i = 1; i < arrayLength; i++) {
                data.push({
                    col0: "Stockquote " + i,
                    col1: Math.floor(Math.random() * 1000) + 1,
                    col2: new Date(minDate.getTime() + Math.random() * (maxDate.getTime() - minDate.getTime())).toDateString(),
                    col3: new Date(minDate.getTime() + Math.random() * (maxDate.getTime() - minDate.getTime())).toLocaleTimeString(),
                    col4: Math.round((i - 50) * Math.floor(Math.random() * 2) % 10).toString(),
                    col5: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col6: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col7: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col8: (Math.floor(Math.random() * 1000) + 1).toString()
                });
            }
            app.stockdata = data;
            app.quoteContainer.dispatchEvent(app.loadedDataEvent);
            setTimeout(app.randomData, app.settings.refreshDelay);
        },

        clearContainer: function () {
            var container,
                i;
            container = document.querySelector("#container");
            for (i = 0; i < container.childNodes.length; i += 1) {
                container.removeChild(container.childNodes[i]);
            }
        },

        createTable: function () {
            var tableNode,
                rowCount,
                rowNode,
                cellNode,
                property;
            app.clearContainer();
            tableNode = document.createElement("table");
            for (rowCount = 0; rowCount < app.stockdata.length; rowCount += 1) {
                rowNode = document.createElement("tr");
                for (property in app.stockdata[rowCount]) {
                    if (typeof app.stockdata[rowCount][property] === "string") {
                        cellNode = document.createElement("td");
                        cellNode.innerHTML = app.stockdata[rowCount][property];
                        if (property === "col4") {
                            if (app.stockdata[rowCount][property][0] === "-") {
                                rowNode.className += " loser";
                            } else {
                                rowNode.className += " winner";
                            }
                        }
                        rowNode.appendChild(cellNode);
                    }
                }
                tableNode.appendChild(rowNode);
            }
            app.quoteContainer.appendChild(tableNode);
            return tableNode;
        },

        init: function () {


            var titleNode, postFormNode, dataSourceLabel, dataSourceInput, ajaxOption, websocketOption, generatedDataOption, postFormSubmit;

            app.dataSource();

            titleNode = document.createElement("h1");
            titleNode.innerHTML = "Realtime Stockquotes";
            document.body.appendChild(titleNode);

            postFormNode = document.createElement("form");
            postFormNode.action = "index.html";
            postFormNode.method = "get";

            dataSourceLabel = document.createElement("label");
            dataSourceLabel.innerHTML = "You can change the source of the stock data with this form: ";

            dataSourceInput = document.createElement("select");
            dataSourceInput.name = "dataSource";

            ajaxOption = document.createElement("option");
            ajaxOption.text = "ajax";

            websocketOption = document.createElement("option");
            websocketOption.text = "websocket";

            generatedDataOption = document.createElement("option");
            generatedDataOption.text = "generatedData";

            dataSourceInput.options.add(ajaxOption, 1);
            dataSourceInput.options.add(websocketOption, 2);
            dataSourceInput.options.add(generatedDataOption, 3);

            postFormNode.appendChild(dataSourceLabel);
            postFormNode.appendChild(dataSourceInput);

            postFormSubmit = document.createElement("input");
            postFormSubmit.type = "submit";
            postFormSubmit.value = "Submit";
            postFormNode.appendChild(postFormSubmit);

            document.body.appendChild(postFormNode);

            app.quoteContainer = document.createElement("div");
            app.quoteContainer.id = "container";
            app.loadedDataEvent = document.createEvent("Event");
            app.loadedDataEvent.initEvent("dataloaded", true, true);
            app.quoteContainer.addEventListener("dataloaded", app.createTable);
            document.body.appendChild(app.quoteContainer);
            app.loadDataSource();
        }

    };
    app.init();
}());