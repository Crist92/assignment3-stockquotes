Document all concepts and your implementation decisions.

---


#Return values from yahoo
Values available in the test urls.

    s   Symbol
    l1  Last Trade (Price Only)
    d1  Date of Last Trade
    t1  Time of Last Trade
    c1  Change (in points)
    o   Open price
    h   Day’s High
    g   Day’s Low
    v   Volume

All return values

    a	Ask
    a2	Average Daily Volume
    a5	Ask Size
    b	Bid
    b2	Ask (Real-time)
    b3	Bid (Real-time)
    b4	Book Value
    b6	Bid Size
    c	Change and Percent Change
    c1	Change
    c3	Commission
    c6	Change (Real-time)
    c8	After Hours Change (Real-time)
    d	Dividend/Share
    d1	Last Trade Date
    d2	Trade Date
    e	Earnings/Share
    e1	Error Indication (returned for symbol changed / invalid)
    e7	EPS Est. Current Yr
    e8	EPS Estimate Next Year
    e9	EPS Est. Next Quarter
    f6	Float Shares
    g	Day’s Low
    g1	Holdings Gain Percent
    g3	Annualized Gain
    g4	Holdings Gain
    g5	Holdings Gain Percent (Real-time)
    g6	Holdings Gain (Real-time)
    h	Day’s High
    i	More Info
    i5	Order Book (Real-time)
    j	52-week Low
    j1	Market Capitalization
    j3	Market Cap (Real-time)
    j4	EBITDA
    j5	Change From 52-week Low
    j6	Percent Change From 52-week Low
    k	52-week High
    k1	Last Trade (Real-time) With Time
    k2	Change Percent (Real-time)
    k3	Last Trade Size
    k4	Change From 52-wk High
    k5	Percent Change From 52-week High
    l	Last Trade (With Time)
    l1	Last Trade (Price Only)
    l2	High Limit
    l2	High Limit
    l3	Low Limit
    l3	Low Limit
    m	Day's Range
    m2	Day’s Range (Real-time)
    m3	50-day Moving Avg
    m4	200-day Moving Average
    m5	Change From 200-day Moving Avg
    m6	Percent Change From 200-day Moving Average
    m7	Change From 50-day Moving Avg
    m8	Percent Change From 50-day Moving Average
    n	Name
    n4	Notes
    o	Open
    p	Previous Close
    p1	Price Paid
    p2	Change in Percent
    p5	Price/Sales
    p6	Price/Book
    q	Ex-Dividend Date
    q	Ex-Dividend Date
    r	P/E Ratio
    r1	Dividend Pay Date
    r2	P/E (Real-time)
    r5	PEG Ratio
    r6	Price/EPS Est. Current Yr
    r7	Price/EPS Estimate Next Year
    s	Symbol
    s1	Shares Owned
    s7	Short Ratio
    s7	Short Ratio
    t1	Last Trade Time
    t6	Trade Links
    t7	Ticker Trend
    t8	1 yr Target Price
    v	Volume
    v1	Holdings Value
    v7	Holdings Value (Real-time)
    v7	Holdings Value (Real-time)
    w	52-week Range
    w	52-week Range
    w1	Day's Value Change
    w1	Day’s Value Change
    w4	Day's Value Change (Real-time)
    w4	Day’s Value Change (Real-time)
    x	Stock Exchange
    x	Stock Exchange
    y	Dividend Yield
    y	Dividend Yield

### Get the company name with a ticker symbol
    http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=TICKER_SYMBOL&callback=YAHOO.Finance.SymbolSuggest.ssCallback

Replace `TICKER_SYMBOL` with your ticket symbol.


#Flow of the program

1. initialize
2. get the data source
3. draw the interface
4. load the data
5. refresh every 3 seconds


#Concepts
For every concept the following items:

- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritative and authentic)

###Objects, including object creation and inheritance
Object Oriented Programming is a way of programming that uses Objects to model real world objects that you find in everyday life.
These objects contain certain properties, in the form of attributes and they can handle certain procedures in the form of methods.

Creating an object:
var o = {a: 1, b: 2};

Inheritance in OO-JavaScript:
Inheritance in Object Ortiented JavaScript works through a prototype chain. We create a prototype off the object created in the previous paragraph,
and give it a property named c.

var o.prototype = {c: 3};

Now if we try to print each of these properties on the screen, the javascript console will first check the object at the top of the prototype chain
if it contains the property we are trying to print. If it does it will print it right away, if it doesn't it will check the next prototype of o in
the prototype chain.

console.log(o.a);
Will print 1 as o contains a property of a with the value of 1.

console.log(o.b);
Will print 2 as o contains a property of b with the value of 2.

console.log(o.c);
Will check for a property named c on the object o, it won't find the property so it'll continue it's search in the next object in the prototype chain.
o.prototype contains a property named c with the value of 3. So it will print 3.

Mandatory documentation: https://online.han.nl/sites/5-ICA-CRIA-50044/CRIA-VT-WT/lesmaterialen1/SHEETS/Part%2005%20-%20Objectorientation/Onderdeel%206%20-%20Reader.pdf
Alternative documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain#Inheriting_properties

###websockets
WebSocket is a protocol providing full-duplex communications channels over a single connection. This allows the client to send data to the server,
and the server to send data to the client without a request from the client.

socket: io("http://server7.tezzt.nl:1333"),
Sets the socket connection to http://server7.tezzt.nl:1333.

app.socket.on('stockquotes', function (data) {
    app.stockdata = data.query.results.row;
});
Retrieves the data from the WebSocket and places it into an array for further use.

Mandatory documentation: http://www.html5rocks.com/en/tutorials/websockets/basics/
Alternative documentation: http://en.wikipedia.org/wiki/WebSocket


###XMLHttpRequest
The XMLHttpRequest object is used to exchange data with a server behind the scene.
This is very convenient as it allows you to update a page with reloading it, request and recieve data from a server after the page has loaded and
send data to the server in the background.

createXMLHttpRequest: function (method, url) {
            var request;
            request = new XMLHttpRequest();
            if (request.withCredentials !== undefined) {
                request.open(method, url, true);
            } else {
                request = null;
            }
            return request;
        },
Creates a new XMLHttpRequest object and checks for the credentials to be defined before returning the object.

Mandatory documentation: Unable to find mandatory documentation about XMLHttpRequest.
Alternative documentation: http://www.w3schools.com/xml/xml_http.asp

###AJAX
AJAX allows web pages to be updated asynchronously by exchanging small amounts of data with the server behind the scenes. This means that it is
possible to update parts of a web page, without reloading the whole page. To do this AJAX makes use of the XMLHttpRequest discussed in the previous
paragraph.

request = app.createXMLHttpRequest('GET', app.ajaxUrl);
Uses the function created in the previous paragraph to create a new XMLHttpRequest object using the GET method and the url to the AJAX data.

###Callbacks
A callback function is a function that is passed on as a parameter to another function, to be executed later in this particular function.

function mySandwich(param1, param2, callback) {
    alert('Started eating my sandwich.\n\nIt has: ' + param1 + ', ' + param2);
    callback();
}

mySandwich('ham', 'cheese', function() {
    alert('Finished eating my sandwich.');
});

The function mySandwich needs 3 parameters, the first two will be used in the alert message on the first line of the function. The third is the
callback function. This gets executed after the alert message.

Mandatory documentation: Unable to find mandatory documentation on callback functions.
Alternative documentation: http://www.impressivewebs.com/callback-functions-javascript/

###How to write testable code for unit-tests
The way to write testable code for unit-tests is by creating standalone functions that do not rely on other functions in the program. By letting these
functions return their results you can use a Behavior-Driven Javascript Framework (like Jasmine) to write unit tests that execute the functions and
lets you predict the outcome. This way you can test your written functions.

describe("app", function () {
    beforeEach(function () {
        app.initialize();
        app.generateRandomData();
    });

    it("websocket is supposed to be connected to http://server7.tezzt.nl:1333",function(){
        expect(app.socket.io.uri).toBe("http://server7.tezzt.nl:1333");
    });
});

This piece of code tests if the websocket is connected to the right url. Before it does anything it first initializes the application. Then it checks
for the value of app.socket.io.uri to be the right url.

Mandatory documentation: Unable to find mandatory documentation on unit tests.
Alternative documentation: http://jasmine.github.io/2.0/introduction.html