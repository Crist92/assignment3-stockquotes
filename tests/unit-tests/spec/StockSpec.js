describe("app", function () {
    beforeEach(function () {
        app.init();
        app.randomData();
    });

    it("websocket is supposed to be connected to http://server7.tezzt.nl:1333",function(){
        expect(app.socket.io.uri).toBe("http://server7.tezzt.nl:1333");
    });
});

describe("XMLHttpRequest", function () {
    it("The request is supposed to be an instance of XMLHttpRequest", function () {
        var request = app.XMLHttpRequest("GET", "www.google.nl")
        expect(request).not.toBe(undefined);
        expect(request instanceof XMLHttpRequest).toBe(true);
    });
});

describe("createTable", function () {
    it("The document is supposed to contain 24 table rows", function () {
        expect(document.body.querySelectorAll("tr").length).toBe(24);
    });
});

describe("generateTestData", function () {

    it("stockdata is supposed to contain 24 items", function () {
        expect(app.stockdata.length).toBe(24);
    });
});

describe("responseHandler", function () {
    var json = '{"query": {"count": 27,"created": "2015-03-15T18:05:19+01:00","lang": "en-US","diagnostics": {"publiclyCallable": "true","url": {"execution-start-time": "1","execution-stop-time": "67","execution-time": "66","content": "http:\/\/download.finance.yahoo.com\/d\/quotes.csv?s=ibm+orcl+bcs+stt+jpm+lgen.l+ubs+db+ben+cs+bk+kn.pa+gs+lm+ms+mtu+ntrs+gle.pa+bac+av+sdr.l+dodgx+slf+sl.l+nmr+ing+bnp.pa&f=sl1d1t1c1ohgv&e\u200c?=.csv"},"user-time": "68","service-time": "66","build-version": "0.2.1867"},"results": {"row": [{"col0": "BCS","col1": "18.81","col2": "03\/15\/2015","col3": "06:05pm","col4": "1.39","col5": "N\/A","col6": "N\/A","col7": "N\/A","col8": "8900"},{"col0": "STT","col1": "62.03","col2": "03\/15\/2015","col3": "06:05pm","col4": "-4.67","col5": "N\/A","col6": "N\/A","col7": "N\/A","col8": "0"}]}}}';
    var event = {
        target: {
            responseText: json
        }
    }

    it("responseHandler is supposed to trigger loadedDataEvent", function () {
        var eventTriggered = false;
        app.quoteContainer.addEventListener("dataloaded", function () {
            eventTriggered = true;
        });
        app.responseHandler(event);
        expect(eventTriggered).toBe(true);
    });
});